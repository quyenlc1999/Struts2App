<%--
  Created by IntelliJ IDEA.
  User: quyenlc2
  Date: 8/26/2022
  Time: 11:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Danh sách liên hệ</title>
    <style>

        button{
            cursor: pointer;
        }

        table, td, th {
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            /*border: 1px solid black;*/
        }

        table {
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        .contact-table {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        th, td {
            padding: 10px;
        }

        th {
            height: 30px;
        }

        tr:nth-child(even) {
            background: #FFF;
        }

        tr:nth-child(odd) {
            background: #CCC;
        }

        .button-search {
            background-color: green;
            color: white;
            height: 30px;
            border: none;
            border-radius: 2px;
        }

        .button-reset {
            background-color: grey;
            color: white;
            height: 30px;
            border: none;
            border-radius: 2px;
        }

        .button-add {
            background-color: green;
            color: white;
            height: 30px;
            margin-bottom: 10px;
            float: right;
            border: none;
            border-radius: 2px;
        }

        .button-update {
            background-color: #008CBA;
            color: white;
            height: 30px;
            border: none;
            border-radius: 2px;
        }

        .button-delete {
            background-color: red;
            color: white;
            height: 30px;
            border: none;
            border-radius: 2px;
        }

        /*Pagination*/
        #pagination {
            display: block;
            float: right;
        }

        #pagination button {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
            margin-right: 5px;
        }

        #pagination button {
            background-color: #e1e8e1;
            /*background-color: #4CAF50;*/
            color: black;
            border-radius: 5px;
            border: grey;
        }

        #pagination button:hover:not(.active) {
            background-color: #ddd;
            border-radius: 5px;
        }

        .active, .btn:hover {
            background-color: #666;
            color: white;
        }

        .table-footer {
            margin-top: 8px;
        }

        .total-record {
            float: left;
            font-weight: bold;
            font-size: 18px;
        }

        /*CSS Modal confirm delete*/
        /* Float cancel and delete buttons and add an equal width */
        .cancelbtn, .deletebtn {
            float: left;
            width: 50%;
        }

        /* Add a color to the cancel button */
        .cancelbtn {
            background-color: #ccc;
            color: black;
            height: 30px;
            border: none;
            border-radius: 2px;
        }

        /* Add a color to the delete button */
        .deletebtn {
            background-color: #f44336;
            height: 30px;
            border: none;
            border-radius: 2px;
        }

        /* Add padding and center-align text to the container */
        .container {
            padding: 16px;
            text-align: center;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 30%;
            top: 0;
            width: 30%; /* Full width */
            height: 40%; /* Full height */
            /*overflow: auto; !* Enable scroll if needed *!*/
            /*background-color: #474e5d;*/
            padding-top: 50px;
            text-align: center;
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
            border: 1px solid #888;
            width: 80%; /* Could be more or less, depending on screen size */
        }

        /* Style the horizontal ruler */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* The Modal Close Button (x) */
        /*.close {*/
        /*    position: absolute;*/
        /*    right: 35px;*/
        /*    top: 15px;*/
        /*    font-size: 40px;*/
        /*    font-weight: bold;*/
        /*    color: #f1f1f1;*/
        /*}*/

        /*.close:hover,*/
        /*.close:focus {*/
        /*    color: #f44336;*/
        /*    cursor: pointer;*/
        /*}*/

        /* Clear floats */
        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }

        /* Change styles for cancel button and delete button on extra small screens */
        @media screen and (max-width: 300px) {
            .cancelbtn, .deletebtn {
                width: 100%;
            }
        }


    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        // $(document).ready(function(){
        //     if($('.message-status').val() != null && $('.message-status').val() != ''){
        //         $(".message-status").show()
        //         setTimeout(function() {
        //             $(".message-status").hide()
        //         }, 5000);
        //     }
        // });
        function search(page) {
            var rowPerPage = 10;
            var full_name = $("#full_name").val();
            window.location.href = "search.action?fullName=" + full_name + "&page=" + page + "&rowPerPage=" + rowPerPage;
        }

        function reset() {
            $("#full_name").val("");
            var full_name = "";
            var page = 1;
            var rowPerPage = 10;
            window.location.href = "search.action?fullName=" + full_name + "&page=" + page + "&rowPerPage=" + rowPerPage;
        }

        function viewFormAdd() {
            window.location.href = "viewFormAdd.action";
        }

        function showModalDelete(id,fullName) {
            $("#full-name-delete").val(fullName);
            var full_name = $("#full-name-delete").val();
            console.log(full_name);
            $(".deletebtn").val(id);
            $("#modal-delete-confirm").css("display", "block");
        }

        function cancelDelete() {
            $("#modal-delete-confirm").css("display", "none");
        }

        function confirmDelete() {
            window.location.href = "delete.action?id=" + $(".deletebtn").val();
            $("#modal-delete-confirm").css("display", "none");
        }

        function previousPage(currentPage) {
            var page = currentPage - 1;
            var rowPerPage = 10;
            var full_name = $("#full_name").val();
            window.location.href = "search.action?fullName=" + full_name + "&page=" + page + "&rowPerPage=" + rowPerPage;
        }

        function lastPage(currentPage) {
            var page = currentPage + 1;
            var rowPerPage = 10;
            var full_name = $("#full_name").val();
            window.location.href = "search.action?fullName=" + full_name + "&page=" + page + "&rowPerPage=" + rowPerPage;
        }
        // var header = document.getElementById("pagination");
        var btns = document.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                if (current.length > 0) {
                    current[0].className = current[0].className.replace(" active", "");
                }
                // current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }
    </script>
</head>
<body>
<%--<s:a action="listView">Hiển thị danh sách</s:a>--%>
<h2>Danh sách liên hệ</h2>
<%--<div class="message-status">--%>
<%--    <s:if test ="#session.messageAddSuccess =! null && #session.messageAddSuccess != ''">--%>
<%--        <s:property value="#session.messageAddSuccess"/>--%>
<%--    </s:if>--%>
<%--</div>--%>
<div>
    <label style="font-weight: bold;">Họ và tên</label>
    <s:textfield name="fullName" id="full_name" cssStyle="margin-left: 15px;height: 30px;"/>
    <button class="button-search" onclick="search(1)">Tìm kiếm</button>
    <button class="button-reset" onclick="reset()">Làm mới</button>
    <%--    <button class="button-search">--%>
    <%--    <a onsubmit="search()" style="text-decoration: none;">Tìm kiếm</a>--%>
    <%--    &lt;%&ndash;        <a href="search.action?fullName=<s:property value="fullName"/>" style="text-decor   ation: none;">Tìm kiếm</a>&ndash;%&gt;--%>
    <%--</button>--%>
</div>

<div>
    <button class="button-add" onclick="viewFormAdd()">Thêm mới</button>
    <table class="contact-table">
        <tr>
            <th>Họ và tên</th>
            <th>Email</th>
            <th>Điện thoại</th>
            <th>Ngày sinh</th>
            <th>Địa chỉ</th>
            <th></th>
        </tr>
        <s:if test="contactDTO.contactList.size() > 0">
            <s:iterator value="contactDTO.contactList" var="contact">
                <tr>
                    <td><s:property value="fullName"/></td>
                    <td><s:property value="emailId"/></td>
                    <td><s:property value="cellNo"/></td>
                    <td>
                        <s:date name="birthDate" format="dd/MM/yyyy"></s:date>
<%--                        <s:property value="birthDate"/>--%>
                    </td>
                    <td><s:property value="website"/></td>
                    <td><a href="updateView.action?id=<s:property value="id"/>">
                        <button class="button-update">Cập nhật</button>
                    </a>

                            <%--                        <a href="delete.action?id=<s:property value="id"/>">--%>
                        <button class="button-delete"
                                onclick="showModalDelete(<s:property value="id"/>,'<s:property value="fullName"/>')">Xóa
                        </button>
                            <%--                        </a>--%>
                    </td>
                </tr>
            </s:iterator>


        </s:if>
        <s:else>
            <tr>
                <td colspan="6">Không có dữ liệu tìm kiếm</td>
            </tr>
        </s:else>

    </table>

    <s:if test="contactDTO.contactList.size() > 0">
        <div class="table-footer">
            <div class="total-record">Tổng số dữ liệu: <s:property value="contactDTO.totalRecord"/></div>

            <div id="pagination">
                <s:if test="contactDTO.pageNumber > 0 && contactDTO.page != 1">
                    <button onclick="previousPage(${contactDTO.page})">&laquo;</button>
                </s:if>
                <s:if test="contactDTO.pageNumber > 0">
                    <c:forEach var="i" begin="1" end="${contactDTO.pageNumber}">
                        <button style="border: none;" class="btn" onclick="search(${i})">${i}</button>
                    </c:forEach>
                </s:if>
                <s:if test="contactDTO.pageNumber > 0 && contactDTO.page != contactDTO.pageNumber">
                    <button onclick="lastPage(${contactDTO.page})">&raquo;</button>
                </s:if>
            </div>
        </div>
    </s:if>
</div>

<div id="modal-delete-confirm" class="modal">
    <form class="modal-content">
        <div class="container">
            <h1>Xóa tài khoản</h1>
                <p>Bạn có chắc chắn muốn xóa tài khoản này không?</p>
            <p></p>
            <div class="clearfix">
                <button type="button" class="deletebtn" onclick="confirmDelete()">Xóa</button>
                <button type="button" class="cancelbtn" onclick="cancelDelete()">Hủy bỏ</button>
            </div>
        </div>
    </form>
</div>


</body>
</html>
