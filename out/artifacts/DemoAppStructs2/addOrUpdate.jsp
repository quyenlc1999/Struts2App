<%--
  Created by IntelliJ IDEA.
  User: quyenlc2
  Date: 8/25/2022
  Time: 8:28 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<html>
<head>
    <s:if test="action=='ADD'">
        <title>Thêm mới tài khoản người dùng</title>
    </s:if>
    <s:else>
        <title>Cập nhật tài khoản người dùng</title>
    </s:else>
    <style>

        /*.form-input{*/
        /*    margin-top: 10px;*/
        /*}*/
        /*.label-title{*/
        /*    width: 300px;*/
        /*    margin-right: 30px;*/
        /*}*/
        /*.validate-error{*/
        /*    height: 30px;*/
        /*    width: fit-content;*/
        /*    border: 1px solid grey;*/
        /*}*/
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/dojo/1.8.1/dojo/dojo.js"
            data-dojo-config="async: true"></script>
    <script>
        $(document).ready(function(){
            var validate_value = $(".validate-error").val();
            if(validate_value != ""){
                // Delay the action by 10000ms
                setTimeout(function(){
                    // Display the div containing the class "validate-error"
                    $(".validate-error").show();
                }, 10000);
            }
        });

        function valdidateContacts() {
            var validate_value = $(".validate-error").val();
            if(validate_value != ""){
                // Delay the action by 10000ms
                setTimeout(function(){
                    // Display the div containing the class "validate-error"
                    $(".validate-error").show();
                }, 10000);
            }
        }
    </script>
    <sx:head />
</head>
<body>

<s:if test="action=='ADD'">
    <h1>Thêm mới tài khoản người dùng</h1>
</s:if>
<s:else>
    <h1>Cập nhật tài khoản người dùng</h1>
</s:else>
<s:actionerror/>

<s:if test="action=='ADD'">
    <s:form action="add" method="post">
        <s:textfield name="contact.fullName" label="Họ và tên"/>
        <div class="validate-error">
        <s:property value="contactValidate.fullName"/>
        </div>
        <s:textfield name="contact.emailId" label="Email"/>
        <s:property value="contactValidate.emailId"/>
        <s:textfield name="contact.cellNo" label="Điện thoại"/>
        <s:property value="contactValidate.cellNo"/>
        <s:textfield name="contact.website" label="Địa chỉ"/>
        <s:property value="contactValidate.website"/>
        <sx:datetimepicker name="contact.birthDate" label="Ngày sinh" displayFormat="dd/MMM/yyyy"/>
        <s:property value="contactValidate.birthDate"/>
        <s:submit value="Thêm mới" align="center" onclick="valdidateContacts()"/>
    </s:form>
<%--    <form action="add.action" method="post">--%>
<%--        <label>Họ và tên</label>--%>
<%--        <input name="contact.fullName"/>--%>
<%--        <label>Email</label>--%>
<%--        <input name="contact.emailId"/>--%>
<%--        <label>Điện thoại</label>--%>
<%--        <input name="contact.cellNo"/>--%>
<%--        <label>Địa chỉ</label>--%>
<%--        <input name="contact.website"/>--%>
<%--        <label>Ngày sinh</label>--%>
<%--        <input name="contact.birthDate"/>--%>
<%--        <button type="submit">Thêm mới</button>--%>
<%--    </form>--%>

</s:if>

<s:elseif test="action=='UPDATE'">
    <s:form action="update" method="post">
        <s:hidden name="contact.id"/>
        <s:textfield name="contact.fullName" label="Họ và tên"/>
        <s:property value="contactValidate.fullName"/>
        <s:textfield name="contact.emailId" label="Email"/>
        <s:property value="contactValidate.emailId"/>
        <s:textfield name="contact.cellNo" label="Điện thoại"/>
        <s:property value="contactValidate.cellNo"/>
        <s:textfield name="contact.website" label="Địa chỉ"/>
        <s:property value="contactValidate.website"/>
        <sx:datetimepicker name="contact.birthDate" label="Ngày sinh" displayFormat="dd/MMM/yyyy"/>
        <s:property value="contactValidate.birthDate"/>
        <s:submit value="Cập nhật" align="center"/>
    </s:form>
</s:elseif>

</body>
</html>
