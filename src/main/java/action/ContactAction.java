package main.java.action;

import com.opensymphony.xwork2.ActionSupport;
import main.java.controller.ContactManagerController;
import org.apache.struts2.ServletActionContext;

import java.util.List;
import java.util.regex.Pattern;

public class ContactAction extends ActionSupport {
    private model.Contact contact;
    private Long id;

    private ContactManagerController controller = new ContactManagerController();

    public void setController(ContactManagerController controller) {
        this.controller = controller;
    }

    public ContactManagerController getController() {
        return controller;
    }

    private List<model.Contact> contactList;
    private String action;
    private String fullName = "";
    private int page;
    private int rowPerPage;
    private int totalRecord;
    private dto.ContactDTO contactDTO;
    private util.PageUtil pageUtil = new util.PageUtil();
    private dto.ContactValidate contactValidate = new dto.ContactValidate();
    private Pattern pattern;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String PHONE_NUMBER = "[\\+84|84|0]+(3|5|7|8|9|1[2|6|8|9])+[0-9]{8}";



    public boolean validateContact() {
        if (contact.getFullName() == null || contact.getFullName().trim().equals("")) {
            contactValidate.setFullName("Trường Họ và tên không được phép để trống");
            return false;
        }
        if (contact.getEmailId() == null || contact.getEmailId().equals("")) {
            contactValidate.setEmailId("Trường Email không được phép để trống");
            return false;
        } else {
            pattern = Pattern.compile(EMAIL_PATTERN);
            if (pattern.matcher(contact.getEmailId()).matches() == false) {
                contactValidate.setEmailId("Trường Email không đúng định đạng");
                return false;
            }
        }
        if (contact.getCellNo() == null || contact.getCellNo().trim().equals("")) {
            contactValidate.setCellNo("Trường Số điện thoại không được phép để trống");
            return false;
        } else {
//            pattern = Pattern.compile(PHONE_NUMBER);
            if (Pattern.compile(PHONE_NUMBER).matcher(contact.getCellNo()).matches() == false) {
                contactValidate.setCellNo("Trường Số điện thoại không đúng định dạng");
                return false;
            }
        }
        if (contact.getWebsite() == null || contact.getWebsite().equals("")) {
            contactValidate.setWebsite("Trường Địa chỉ không được phép để trống");
            return false;
        }
        if (contact.getBirthDate() == null) {
            contactValidate.setBirthDate("Trường Ngày sinh không được phép để trống");
            return false;
        }
        return true;
    }

    public String execute(){
        page = 1;
        rowPerPage = 10;
        this.contactDTO = controller.searchContacts(fullName, 1, 10);
        this.contactDTO.setPageNumber(pageUtil.pageNumber(contactDTO.getTotalRecord(), contactDTO.getRowPerPage()));
        return SUCCESS;
    }

    public String search() {
        if(page <= 0){
            page = 1;
        }if (rowPerPage <= 0){
            rowPerPage = 10;
        }
        this.contactDTO = controller.searchContacts(fullName, page, rowPerPage);
        this.contactDTO.setPageNumber(pageUtil.pageNumber(contactDTO.getTotalRecord(), contactDTO.getRowPerPage()));
        return SUCCESS;
    }

    public String add() {
//        HttpServletRequest request= ServletActionContext.getRequest();
//        HttpSession session = request.getSession();
//        String s = (String) session.getAttribute("Thêm mới");
//        System.out.println(s);
        action = "ADD";
        if (validateContact()) {
            controller.add(getContact());
            this.contactDTO = controller.searchContacts(fullName, 1, 10);
            this.contactDTO.setPageNumber(pageUtil.pageNumber(contactDTO.getTotalRecord(), contactDTO.getRowPerPage()));
            ServletActionContext.getRequest().getSession().setAttribute("messageAddSuccess","Thêm mới thông tin danh sách thành công");
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    public String delete() {
        controller.delete(id);
        this.contactDTO = controller.searchContacts(fullName, 1, 10);
        this.contactDTO.setPageNumber(pageUtil.pageNumber(contactDTO.getTotalRecord(), contactDTO.getRowPerPage()));
        return SUCCESS;
    }

    public String listView() {
        if(page <= 0){
            page = 1;
        }if (rowPerPage <= 0){
            rowPerPage = 10;
        }
        this.contactDTO = controller.searchContacts(fullName, page, rowPerPage);
        this.contactDTO.setPageNumber(pageUtil.pageNumber(contactDTO.getTotalRecord(), contactDTO.getRowPerPage()));
        return SUCCESS;
    }

    public String viewFormAdd() {
        action = "ADD";
        return SUCCESS;
    }

    public String updateView() {
        // update hành động
        action = "UPDATE";
        contact = controller.getContactById(id);
        return SUCCESS;
    }

    public String update() {
        action = "UPDATE";
        if(validateContact()){
            controller.update(contact);
            this.contactDTO = controller.searchContacts(fullName, 1, 10);
            this.contactDTO.setPageNumber(pageUtil.pageNumber(contactDTO.getTotalRecord(), contactDTO.getRowPerPage()));
            return SUCCESS;
        }else {
            return ERROR;
        }
    }

    public model.Contact getContact() {
        return contact;
    }

    public void setContact(model.Contact contact) {
        this.contact = contact;
    }

    public List<model.Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<model.Contact> contactList) {
        this.contactList = contactList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRowPerPage() {
        return rowPerPage;
    }

    public void setRowPerPage(int rowPerPage) {
        this.rowPerPage = rowPerPage;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public dto.ContactDTO getContactDTO() {
        return contactDTO;
    }

    public void setContactDTO(dto.ContactDTO contactDTO) {
        this.contactDTO = contactDTO;
    }

    public util.PageUtil getPageUtil() {
        return pageUtil;
    }

    public void setPageUtil(util.PageUtil pageUtil) {
        this.pageUtil = pageUtil;
    }

    public dto.ContactValidate getContactValidate() {
        return contactValidate;
    }

    public void setContactValidate(dto.ContactValidate contactValidate) {
        this.contactValidate = contactValidate;
    }
}
