package main.java.controller;

import dto.ContactDTO;
import model.Contact;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ContactManagerController extends HibernateUtil {

    public Contact add(Contact contact){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        try{
            session.save(contact);
            session.getTransaction().commit();
        }catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally {
            session.close();
        }

        return contact;
    }

    public Contact update(Contact contact){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        try {
            session.update(contact);
            session.getTransaction().commit();
        }catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally {
            session.close();
        }
        return contact;
    }

    public Contact delete(Long id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Contact contact = null;
        try{
            contact = (Contact) session.load(Contact.class ,id);
            if(contact != null){
                session.delete(contact);
            }
            session.getTransaction().commit();
        }catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally {
            session.close();
        }
        return contact;
    }


    public List<Contact> list(){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Contact> contacts = new ArrayList<Contact>();
        try{
            contacts = (List<Contact>) session.createQuery("from Contact").list();
            session.getTransaction().commit();
        }catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally {
            session.close();
        }
        return contacts;
    }

    public Contact getContactById(Long id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Contact contact = null;
        try{
            contact = (Contact) session.get(Contact.class,id);
            session.getTransaction().commit();
        }catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally {
            session.close();
        }
        return contact;
    }

    public ContactDTO searchContacts(String full_name,int page, int rowPerPage){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        ContactDTO contactDTO = new ContactDTO();
        List<Contact> contacts = new ArrayList<Contact>();
        Long totalRecord = 0L;
        try{
            int offset = (page-1) * rowPerPage;
            String stringFilter = "where fullName like '%"+full_name+"%' order by fullName asc";

            totalRecord = (Long) session.createQuery("select count(c.id) from Contact c where c.fullName like '%"+full_name+"%'").getSingleResult();
            contacts = session.createQuery("from Contact "+stringFilter+"").setFirstResult(offset).setMaxResults(rowPerPage).getResultList();
            contactDTO.setContactList(contacts);
            contactDTO.setPage(page);
            contactDTO.setRowPerPage(rowPerPage);
            contactDTO.setTotalRecord(totalRecord.intValue());

        }catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }finally {
            session.close();
        }
        return contactDTO;
    }

}
