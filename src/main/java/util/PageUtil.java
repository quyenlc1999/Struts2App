package util;

public class PageUtil {
    public int pageNumber(int totalRecord , int rowPerPage){
        int result = 0;
        if(totalRecord % rowPerPage == 0){
            result = totalRecord / rowPerPage;
        }else {
            result = totalRecord / rowPerPage + 1;
        }
        return result;
    }
}
